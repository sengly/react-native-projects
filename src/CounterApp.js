import React, {Component} from 'react';
import {Text, View, Button, StyleSheet} from 'react-native';
import {connect} from 'react-redux'

class Counter extends Component {
  

  increaseCounter = () => {
    this.setState({counter: this.state.counter + 1});
  };

  decreaseCounter = () => {
    this.setState({counter: this.state.counter - 1});
  };

  render() {
    return (
      <View style={style.container}>
        <Button title="Increase" onPress={this.increaseCounter} />
        <Text>{this.state.counter}</Text>
        <Button title="Decrease" onPress={this.decreaseCounter} />
      </View>
    );
  }
}
mapStateToProps = (){
    return{
        counter:this.counter
    }
}
export default (Counter);

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
