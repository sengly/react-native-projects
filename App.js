import React,{Component} from 'react'
import CounterApp from './src/CounterApp'
import {createStore} from 'redux'
import {Provider} from 'react-redux'

const initialState = {
  counter: 0
}

const reducer = (state=initialState)=>{
  return state
}

const store = createStore(reducer)

class App extends Component{

  render(){
    return(
      <Provider store={store}>
        <CounterApp/>
      </Provider>
    )
  }

}

export default App
